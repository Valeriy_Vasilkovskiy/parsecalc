package main;

public class Main {
    public static void main(String[] args) {
        Function f = new Function();

        f.calc(f.parseArray("1.6*50"));
        f.calcObj(f.parseObj("100/35"));
    }
}
