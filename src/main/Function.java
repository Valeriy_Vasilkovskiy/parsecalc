package main;

import java.util.Arrays;

public class Function {
    String[] parseArray(String string) {
        if (string == null) {
            System.out.println("Where is the string?");
            return null;
        } else {
            //===================
            String val1 = null;
            String val2 = null;
            String val3 = null;
            //======================
            char[] temp = string.toCharArray();
            //============================================
            for (int i = 0; i != string.length(); i++) {
                if (val2 != null) {
                    if (val3 == null) {
                        val3 = String.valueOf(temp[i]);
                    } else {
                        val3 += String.valueOf(temp[i]);
                    }
                }
                if (temp[i] == '+' || temp[i] == '-' || temp[i] == '*' || temp[i] == '/') {
                    val2 = String.valueOf(temp[i]);
                }
                if (val2 == null) {
                    if (val1 == null) {
                        val1 = String.valueOf(temp[i]);
                    } else {
                        val1 += String.valueOf(temp[i]);
                    }
                }
            }
            //==========================================
            if (val2 == null) {
                System.out.println("Where is the sign?");
                String[] res = {val1, val2, val3};
                return res;
            }
            String[] res = {val1, val2, val3};
            return res;
        }
    }

    Object parseObj(String string) {
        if (string == null) {
            System.out.println("Where is the string?");
            return null;
        } else {
            Res result = new Res();
            char[] temp = string.toCharArray();
            for (int i = 0; i != string.length(); i++) {
                if (result.oper != null) {
                    if (result.val2 == null) {
                        result.val2 = String.valueOf(temp[i]);
                    } else {
                        result.val2 += String.valueOf(temp[i]);
                    }
                }
                if (temp[i] == '+' || temp[i] == '-' || temp[i] == '*' || temp[i] == '/') {
                    result.oper = String.valueOf(temp[i]);
                }
                if (result.oper == null) {
                    if (result.val1 == null) {
                        result.val1 = String.valueOf(temp[i]);
                    } else {
                        result.val1 += String.valueOf(temp[i]);
                    }
                }
            }
            if (result.oper == null) {
                System.out.println("Where is the sign?");
                return null;
            } else {
                return result;
            }
        }
    }

    void calc(String[] arr) {
        if (arr[1] == null) {
            return;
        } else {
            double a = parse(arr[0]);
            double b = parse(arr[2]);

            switch (arr[1]) {
                case "+":
                    System.out.print(a + " " + arr[1] + " " + b + " = ");
                    IntOrDouble(a + b);
                    break;
                case "-":
                    System.out.print(a + " " + arr[1] + " " + b + " = ");
                    IntOrDouble(a - b);
                    break;
                case "*":
                    System.out.print(a + " " + arr[1] + " " + b + " = ");
                    IntOrDouble(a * b);
                    break;
                case "/":
                    System.out.print(a + " " + arr[1] + " " + b + " = ");
                    IntOrDouble(a / b);
                    break;
            }
        }
    }
    void calcObj(Object res) {
        Res result = (Res) res;
        if (result.oper == null) {
            return;
        } else {
            double a = parse(result.val1);
            double b = parse(result.val2);
            switch (result.oper) {
                case "+":
                    System.out.print(a + " " + result.oper + " " + b + " = ");
                    IntOrDouble(a + b);
                    break;
                case "-":
                    System.out.print(a + " " + result.oper + " " + b + " = ");
                    IntOrDouble(a - b);
                    break;
                case "*":
                    System.out.print(a + " " + result.oper + " " + b + " = ");
                    IntOrDouble(a * b);
                    break;
                case "/":
                    System.out.print(a + " " + result.oper + " " + b + " = ");
                    IntOrDouble(a / b);
                    break;
            }
        }
    }

    double parse(String string) {
        return Double.parseDouble(string);
    }

    void IntOrDouble(double result) {
        if (result % 1 == 0) {
            int res = (int) result;
            System.out.println(res + " - int");
        } else {
            System.out.println(result + "  - double");
        }
    }
}
